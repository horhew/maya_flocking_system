import maya.cmds as cmds
import maya.OpenMaya as om
import maya.OpenMayaFX as fx
import maya.mel as mel

#---------------------------------------------------------------------------------------------------#
#                                      1. Load plugin
#---------------------------------------------------------------------------------------------------#
try:
    # delete all existing flock-related nodes
    cmds.select('flock_gb_*', replace = True)
    sel = cmds.ls(sl = True)
    cmds.delete(sel)
except: pass

cmds.flushUndo()
cmds.unloadPlugin('flock_node.py')
cmds.loadPlugin('flock_node.py')

#---------------------------------------------------------------------------------------------------#
#                                      2. Particles
#---------------------------------------------------------------------------------------------------#
objects = []

''' Mesh '''
sphere = cmds.polySphere(n = 'flock_gb_sphere', r = 2)[0]
objects.append(sphere)

''' Particles '''
# we have to wrap the call in a try/except block, otherwise Maya throws an error
try: cmds.particleFill(rs = 6, maxX = 1, maxY = 1, maxZ = 1, minX = 0, minY = 0, minZ = 0)
except: pass

''' Rename '''
part = cmds.ls(sl = True)[0]
cmds.nParticle(part, e = True, n = 'flock_gb_particle')
part = cmds.ls(sl = True)[0]
objects.append(part)
cmds.pickWalk(d = 'down')
part = cmds.ls(sl = True)[0]

''' Add/set attributes '''
cmds.addAttr(part, ln = 'rotationPP', dt = 'vectorArray')
cmds.addAttr(part, ln = 'rotationPP0', dt = 'vectorArray')
cmds.addAttr(part, ln = 'attributeName', dt = 'string', )

# particle settings
cmds.setAttr(part + '.ignoreSolverGravity', 1)

# particle render settings
particleRenderType = 6 # 1-multistreak, 2-numeric, 3-points, 6-streak
cmds.setAttr(part + '.particleRenderType', particleRenderType)
cmds.setAttr(part + '.colorInput', 3) # 0-constant, 1-age, 3-speed, 4-acceleration

if particleRenderType in (1, 6):
    # multistreak
    if particleRenderType == 1:
        cmds.addAttr(part, ln = 'multiCount', internalSet = True, at = 'long', min = 1, max = 60, dv = 10)
        cmds.addAttr(part, ln = 'multiRadius', internalSet = True, at = 'float', min = 0, max = 10, dv = 3)
        cmds.setAttr(part + '.multiCount', 3)
        cmds.setAttr(part + '.multiRadius', 0.1)
    # streak / multistreak
    cmds.addAttr(part, ln = 'tailSize', at = 'float', min = -100, max = 100, dv = 1)
    cmds.addAttr(part, ln = 'lineWidth', at = 'long', min = 1, max = 20, dv = 1)
    cmds.setAttr('flock_gb_particleShape.tailSize', 3.0)
    cmds.setAttr('flock_gb_particleShape.lineWidth', 2)
    
    cmds.setAttr(part + '.color[0].color_Color', 1, 0, 0)
    cmds.setAttr(part + '.color[0].color_Position', 0.25)
    cmds.setAttr(part + '.color[0].color_Interp', 1)
    
    cmds.setAttr(part + '.color[1].color_Color', 1, 1, 0)
    cmds.setAttr(part + '.color[1].color_Position', 0.50)
    cmds.setAttr(part + '.color[1].color_Interp', 1)
    
    cmds.setAttr(part + '.color[2].color_Color', 1, 1, 1)
    cmds.setAttr(part + '.color[2].color_Position', 0.75)
    cmds.setAttr(part + '.color[2].color_Interp', 1)
#---------------------------------------------------------------------------------------------------#
#                                      3. Instancer
#---------------------------------------------------------------------------------------------------#

''' Instanced object '''

instObj = cmds.polyCube(n = 'flock_gb_instanceObj', w = 0.5, h = 0.05, d = 0.05)[0]
instObj = cmds.polySphere(n = 'flock_gb_instanceObj', r = 0.1, sx = 8, sy = 8)[0]
inst = cmds.particleInstancer(part, n = 'flock_gb_instancer', addObject = True, object = instObj)
objects.append(instObj)

#---------------------------------------------------------------------------------------------------#
#                                      4. Flock node, targets, obstacles
#---------------------------------------------------------------------------------------------------#                 
''' Flock node'''
flock = cmds.createNode('Flock', n = 'flock_gb_node')
objects.append(flock)

''' Target '''
target1 = cmds.spaceLocator(n = 'flock_gb_target')[0]
cmds.move(15, 0, 0, target1)
objects.append(target1)

''' Obstacles '''
obstacle1 = cmds.polySphere(r = 3, n = 'flock_gb_obstacle')[0]
obstacle2 = cmds.polySphere(r = 3, n = 'flock_gb_obstacle')[0]
cmds.move(7, 0, -4, obstacle1)
cmds.move(7, 0,  4, obstacle2)
objects.append(obstacle1)
objects.append(obstacle2)

#---------------------------------------------------------------------------------------------------#
#                                      5. Connections
#---------------------------------------------------------------------------------------------------#
cmds.connectDynamic(part, fields = flock)
cmds.connectAttr(part +  '.particleId', flock + '.particleId')
cmds.connectAttr(target1 + '.worldMatrix', flock + '.target[0].targetMatrix')
cmds.connectAttr(obstacle1 + '.worldMatrix', flock + '.obstacle[0].obstacleMatrix')
cmds.connectAttr(obstacle1 + '.boundingBoxSize', flock + '.obstacle[0].obstacleBoundingBoxSize')
cmds.connectAttr(obstacle2 + '.worldMatrix', flock + '.obstacle[1].obstacleMatrix')
cmds.connectAttr(obstacle2 + '.boundingBoxSize', flock + '.obstacle[1].obstacleBoundingBoxSize')
cmds.connectAttr(flock + '.maxSpeed', part + '.colorInputMax')

#---------------------------------------------------------------------------------------------------#
#                                      6. Grouping & selection
#---------------------------------------------------------------------------------------------------#
cmds.hide(sphere)
cmds.hide(instObj)
cmds.group(objects, n = 'flock_gb_group')
cmds.select(flock)
    
#---------------------------------------------------------------------------------------------------#
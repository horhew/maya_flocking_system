#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#                                      PLUGIN INFO
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
# DGNode name: Flock
# Version: 1.0
# Date: May 2016
# Description: Flocking behaviour force field.
# Author: Liviu-George Bitiusca
# Credits: Various code snippets taken from Autodesk Maya official documentation examples.
# Based on Craig W. Reynolds' 1987 and 1999 papers.
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

import maya.OpenMaya as om
import maya.OpenMayaMPx as ommpx
import sys, time
import random
from math import cos

helpMessage = 'This node is a simple flocking system. Visit www.georgebit.com or send me an e-mail at'
helpMessage += 'george@georgebit.com for more information.'

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#                                      UTILITY FUNCTIONS
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

def statusError(msg):
    sys.stderr.write('%s\n' %msg)
    raise # called from exception handlers only, reraise exception

def printVec(vec):
    print '<', vec.x, ',', vec.y, ',', vec.z, '>'

def printList(list):
    if len(list) == 0:
        print 'Empty list'
    for i in range(len(list)):
        print 'Elem i:', list[i], '\n'
        
def clamp(value, low, high):
    return max(low, min(value, high))
    
def clampAndNormalizeDistance(dist, min = 0.0, max = 1.0):
    '''-------------------------------------------------------------------------------------
    [float] dist, min, max
    -------------------------------------------------------------------------------------'''
    # 1. clamp distance between min and max
    clampedDistance = clamp(dist, min, max)
    # 2. subtract min in order to start at 0 (in case min > 0)
    unshiftedDistance = clampedDistance - min
    # 3. divide result by (max-min) to get a mapped value between 0 and 1
    if max - min > 0:
        return unshiftedDistance / (max - min)
    return 0

def normalize(vecArray):
    '''-------------------------------------------------------------------------------------
    MVectorArray    vecArray    -> MVectorArray
    ------------------------------------------------------------------------------
    Normalizes the contents of an MVectorArray
    Returns: normalized array.
    -------------------------------------------------------------------------------------'''
    normalizedArray = om.MVectorArray()
    for i in range(vecArray.length()):
        normalizedArray.append(vecArray[i].normal())
    return normalizedArray

def findNeighbours(p, outArray, positions, velocitiesN, maxNeighbours = -1, cohRadius = 1.0, sepRadius = 0.5, fov = 360.0):
    '''-------------------------------------------------------------------------------------
    int                p              -> particle to get neighbours of
    MVectorArray       outArray       -> empty output array
    MVectorArray       positions      -> array of particle positions
    MVectorArray       velocities     -> array of particle velocities (NORMALIZED)
    int                maxNeighbours  -> maximum number of neighbours returned (-1 returns all within radius)
    float              cohRadius      -> cohesion radius around the particle
    float              sepRadius      -> separation radius around the particle (needs to be <= cohRadius)
    float              fov            -> Field-Of-View to choose neighbours from (in degrees)
    ----------------------------------------------------------------------------------------
    Finds "maxNeighbours" neighbours for every particle and stores them in outArray.
    outArray is an array of tuples: outArray = [ (id, vectorTo, distanceTo), ... ]
        
    (!) This function is the bottleneck of the plugin. A more efficient lookup and sorting is needed
    to increase performance. I have tried using bisect() while adding neighbours, in order to maintain a sorted
    list of neighbours based on their distance -as an alternative to calling sort() at the end-, but it was
    just a bit slower than the current algorithm.
    The plugin can handle 24+ FPS on ~80 particles. Anything above that, it starts
    to slow down really bad. I think speed can be improved, but it would still not compare to a C++ version.
    ----------------------------------------------------------------------------------------
    Returns: number of neighbours within sepRadius (numSepNeighbours). Since sepRadius <= cohRadius, 
             we only need to store neighbours within cohRadius. The first numSepNeighbours neighbours of those
             neighbours are those within sepRadius.
    -------------------------------------------------------------------------------------'''    
    
    # if 0 neighbours are required, leave outArray empty and return 0
    if maxNeighbours == 0: return 0

    #--------------------------------------------------------------------------------------
    # Dot product limit (only if fov != 360 or 0)
    #--------------------------------------------------------------------------------------
    # If the fov for selecting neighbours is less than 360 (0), then we need to determine the max value
    # of the dot product between the particle's velocity direction and the direction vector that goes
    # to a neighbour's position starting from the particle's position.
    #
    # Remember that when using normalized vectors, the dot product is the cosine of the angle between them
    # and that it can only go from -1 to 1, having a domain from 0 to PI(180 degrees).
    # We divide the fov by 2.0 because the fov is split in half by the velocity direction, meaning that
    # dot products with vectors on either half need to be within half the full fov's value.
    #--------------------------------------------------------------------------------------
    dotMin = cos(math.radians(fov / 2.0)) if fov not in (0, 360) else -1

    # number of neighbours that are within sep. radius
    numSepNeighbours = 0
    
    # loop over all particles
    for i in range(positions.length()):
    
        # skip the current particle
        if i == p: continue

        # vector and distance to neighbour j
        vectorTo = positions[i] - positions[p]
        distanceTo = vectorTo.length()
        
        # test if neighbour j is within cohRadius
        if distanceTo <= cohRadius:
            # test if within FOV
            if dotMin == -1 or dotMin <= velocitiesN[i] * directionTo:
                # store Neighbour(id, dirTo, distanceTo)
                outArray.append((i, vectorTo, distanceTo))
                # test if also within sepRadius (increment counter if so)
                if distanceTo <= sepRadius: numSepNeighbours += 1
        
    # if we want all the neighbours within cohDistance, simply return now
    if maxNeighbours == -1:
        return numSepNeighbours

    # sort Neighbour list in ascending order by distanceTo
    outArray.sort(key = lambda neighbour : neighbour[2])

    # only keep the requested number of neighbours
    del outArray[maxNeighbours:]
    
    # if numSepNeighbours > maxNeighbours, clamp it down
    if numSepNeighbours > maxNeighbours:
        numSepNeighbours = maxNeighbours
    
    return numSepNeighbours

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#                                      CLASS DEFINITION
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
class Flock(ommpx.MPxFieldNode):
    kNodeName = 'Flock'
    kNodeId = om.MTypeId(0x00001)  # Node identifier (0 - 0x7ffff)
    
    ''' Class attributes '''
    # flocking parameters
    kMinCohesionDist = om.MObject()
    kMinCohesionDistLong = 'minCohesionDist'
    kMinCohesionDistShort = 'minCohDist'
    
    kCohesionDist = om.MObject()
    kCohesionDistLong = 'cohesionDist'
    kCohesionDistShort = 'cohDist'
    
    kSeparationDist = om.MObject()
    kSeparationDistLong = 'separationDistance'
    kSeparationDistShort = 'sepDist'
    
    kMaxNeighbours = om.MObject()
    kMaxNeighboursLong = 'maxNeighbours'
    kMaxNeighboursShort = 'maxNgb'
    
    kMaxSpeed = om.MObject()
    kMaxSpeedLong = 'maxSpeed'
    kMaxSpeedShort = 'maxSpd'
    
    kMaxAcceleration = om.MObject()
    kMaxAccelerationLong = 'maxAcceleration'
    kMaxAccelerationShort = 'maxAccel'
    
    kFov = om.MObject()
    kFovLong = 'fov'
    kFovShort = 'fov'
    
    kSpeedJump = om.MObject()
    kSpeedJumpLong = 'speedJump'
    kSpeedJumpShort = 'spdJump'
    
    # forces
    kCohesionStrength = om.MObject()
    kCohesionStrengthLong = 'cohesionStrength'
    kCohesionStrengthShort = 'cohS'
    
    kAlignmentStrength = om.MObject()
    kAlignmentStrengthLong = 'alignmentStrength'
    kAlignmentStrengthShort = 'aliS'
    
    kSeparationStrength = om.MObject()
    kSeparationStrengthLong = 'separationStrength'
    kSeparationStrengthShort = 'sepS'
    
    kForceFalloffExponent = om.MObject()
    kForceFalloffExponentLong = 'forceFalloffExponent'
    kForceFalloffExponentShort = 'forceExp'
    
    # targets
    kTarget = om.MObject()
    kTargetLong = 'target'
    kTargetShort = 'target'
    
    kTargetWeight = om.MObject()
    kTargetWeightLong = 'targetWeight'
    kTargetWeightShort = 'targetW'
    
    kTargetMatrix = om.MObject()
    kTargetMatrixLong = 'targetMatrix'
    kTargetMatrixShort = 'targetM'
    
    kTargetRadius = om.MObject()
    kTargetRadiusLong = 'targetRadius'
    kTargetRadiusShort = 'targetR'
    
    kTargetArrivalDistance = om.MObject()
    kTargetArrivalDistanceLong = 'targetArrivalDistance'
    kTargetArrivalDistanceShort = 'targetAD'
    
    kTargetArrivalExp = om.MObject()
    kTargetArrivalExpLong = 'targetArrivalExp'
    kTargetArrivalExpShort = 'targetArrivalExp'
    
    kStaticTarget = om.MObject()
    kStaticTargetLong = 'staticTarget'
    kStaticTargetShort = 'staticTarget'
    
    # obstacles    
    kObstacle = om.MObject()
    kObstacleLong = 'obstacle'
    kObstacleShort = 'obst'
    
    kObstacleMatrix = om.MObject()
    kObstacleMatrixLong = 'obstacleMatrix'
    kObstacleMatrixShort = 'obstM'
    
    kObstacleBoundingBoxSize = om.MObject()
    kObstacleBoundingBoxSizeLong = 'obstacleBoundingBoxSize'
    kObstacleBoundingBoxSizeShort = 'obstBBox'
    
    kObstacleDistance = om.MObject()
    kObstacleDistanceLong = 'obstacleDistance'
    kObstacleDistanceShort = 'obstDist'
    
    kObstacleAvoidanceDistance = om.MObject()
    kObstacleAvoidanceDistanceLong = 'obstacleAvoidanceDistance'
    kObstacleAvoidanceDistanceShort = 'obstAvoidanceDist'

    kObstacleSight = om.MObject()
    kObstacleSightLong = 'obstacleSight'
    kObstacleSightShort = 'obstSight'
    
    # particle id (we don't really need it, but might come in handy for future development)
    kParticleId = om.MObject()
    kParticleIdLong = 'particleId'
    kParticleIdShort = 'pId'

    def __init__(self):
        ''' First thing to do: call the __init__() function of the parent class '''
        ommpx.MPxFieldNode.__init__(self)

        ''' Define here any instance variables '''
        self.zeroThreshold = 0.001

        print 'Node of type "Flock" created.'
    
    def compute(self, plug, dataBlock):
        # get outputForce plug (defined by default with MPxFieldNode)
        plugOutputForce = ommpx.cvar.MPxFieldNode_mOutputForce
        
        if plug == plugOutputForce:
            #---------------------------------------------------------------------------------------------------#
            #                                      A. Get MPxFieldNode input values
            #---------------------------------------------------------------------------------------------------#
            
            ''' A.0 Get the logical index of the element this plug (output) refers to. '''
            try: multiIndex = plug.logicalIndex()
            except: statusError('ERROR in plug.logicalIndex().')
            
            ''' A.1 Get inputData plug array '''
            # -> node.inputData
            plugInputData = ommpx.cvar.MPxFieldNode_mInputData # MPlug
            
            # Use outputArrayValue since we do not want to evaluate both inputs,
            # but only the one related to the requested multiIndex.
            # Evaluating both inputs at once would cause a dependency graph loop.
            try: hInputArray = dataBlock.outputArrayValue(plugInputData) # MArrayDataHandle
            except: statusError('ERROR in hInputArray = block.outputArrayValue().')
            
            
            ''' A.2 Jump to corresponding (compound) plug of inputData array and get its value '''
            # -> node.inputData[i]
            try: hInputArray.jumpToElement(multiIndex) # MStatus
            except: statusError('ERROR in hInputArray.jumpToElement().')
            try: hCompound = hInputArray.inputValue() # MDataHandle
            except: statusError('ERROR in hCompound = hInputArray.inputValue().')
            
            
            ''' A.3 Get children of the compound plug '''
            # -> node.inputData[i].inputPositions
            plugInputPositions = ommpx.cvar.MPxFieldNode_mInputPositions # MPlug
            hPosition = hCompound.child(plugInputPositions) # MDataHandle
            oPosition = hPosition.data() # MObject (kVectorArrayData)
            try:
                fnPosition = om.MFnVectorArrayData(oPosition) # MFnVectorArrayData 
                positions = fnPosition.array() # MVectorArray
            except:
                statusError('ERROR in fnPosition.array(). Could not find positions.')
            
            # -> node.inputData[i].inputVelocities
            plugInputVelocities = ommpx.cvar.MPxFieldNode_mInputVelocities # MPlug
            hVelocity = hCompound.child(plugInputVelocities) # MDataHandle
            oVelocity = hVelocity.data() # MObject (kVectorArrayData)
            try:
                fnVelocity = om.MFnVectorArrayData(oVelocity) # MFnVectorArrayData
                velocities = fnVelocity.array() # MVectorArray
            except:
                statusError('ERROR in fnVelocity.array(). Could not find velocities.')

            # -> node.inputData[i].inputMass
            plugInputMass = ommpx.cvar.MPxFieldNode_mInputMass # MPlug
            hMass = hCompound.child(plugInputMass) # MDataHandle
            oMass = hMass.data() # MObject (kDoubleArrayData)
            try:
                fnMass = om.MFnDoubleArrayData(oMass) # MFnDoubleArrayData
                masses = fnMass.array() # MDoubleArray
            except:
                statusError('ERROR in fnMass.array(). Could not find masses.')
                
            # -> node.inputData[i].deltaTime
            plugInputDeltaTime = ommpx.cvar.MPxFieldNode_mDeltaTime # MPlug
            hDeltaTime = hCompound.child(plugInputDeltaTime) # MDataHandle
            deltaTime = hDeltaTime.asTime() # MTime
            
            #---------------------------------------------------------------------------------------------------#
            #                                      B. Get custom input values
            #---------------------------------------------------------------------------------------------------#
            
            ''' B.0 Flocking attributes '''
            # Cohesion distance (aka neighbour search radius)
            hFloat = dataBlock.inputValue(Flock.kMinCohesionDist) # MDataHandle
            minCohesionDist = hFloat.asFloat()
            hFloat = dataBlock.inputValue(Flock.kCohesionDist) # MDataHandle
            cohesionDist = hFloat.asFloat()
            
            # Separation distance
            hFloat = dataBlock.inputValue(Flock.kSeparationDist) # MDataHandle
            separationDist = hFloat.asFloat()
            if separationDist > cohesionDist: separationDist = cohesionDist # sep. needs to be <= than coh.
            
            # Max speed, acceleration
            hFloat = dataBlock.inputValue(Flock.kMaxSpeed) # MDataHandle
            maxSpeed = hFloat.asFloat()
            hFloat = dataBlock.inputValue(Flock.kMaxAcceleration) # MDataHandle
            maxAccel = hFloat.asFloat()
            
            # Max neighbours
            hInt = dataBlock.inputValue(Flock.kMaxNeighbours) # MDataHandle
            maxNeighbours = hInt.asInt()
            
            # Particle FOV
            hFloat = dataBlock.inputValue(Flock.kFov) # MDataHandle
            fov = hFloat.asFloat()
            
            # Speed jump
            hBool = dataBlock.inputValue(Flock.kSpeedJump) # MDataHandle
            speedJump = hBool.asBool()
            
            ''' B.1 Force coefficients '''
            # Faloff exponent (for non-linear falloff)
            hFloat = dataBlock.inputValue(Flock.kForceFalloffExponent) # MDataHandle
            forceExp = hFloat.asFloat()
            
            # Cohesion strength
            hFloat = dataBlock.inputValue(Flock.kCohesionStrength) # MDataHandle
            cohesionStrength = hFloat.asFloat()
            
            # Alignment strength
            hFloat = dataBlock.inputValue(Flock.kAlignmentStrength) # MDataHandle
            alignmentStrength = hFloat.asFloat()
            
            # Separation strength
            hFloat = dataBlock.inputValue(Flock.kSeparationStrength) # MDataHandle
            separationStrength = hFloat.asFloat()
            
            # Obstacle Sight
            hFloat = dataBlock.inputValue(Flock.kObstacleSight) # MDataHandle
            obstacleSight = hFloat.asFloat()
            
            ''' B.2 Array plug values '''
            # Particle Id
            hParticleId = dataBlock.inputValue(Flock.kParticleId) # MDataHandle
            oParticleId = hParticleId.data() # MObject (kDoubleArrayData)
            try:
                fnParticleId = om.MFnDoubleArrayData(oParticleId) # MFnDoubleArrayData
                particleIds = fnParticleId.array() # MDoubleArray
                count = particleIds.length()
            except:
                statusError('ERROR in fnParticleId.array(). Could not find particleId\'s.')
            
            ''' B.3 Compound values / Plug array values'''
            # Targets
            targetPositions = []
            targetPrevPositions = []
            targetWeights = []
            targetArrivalDistances = []
            targetRadii = []
            targetArrivalExponents = []
            targetStatic = []
            
            hArray = dataBlock.inputArrayValue(Flock.kTarget) # MArrayDataHandle
            for i in range(hArray.elementCount()):
                # -> node.target[i]
                hArray.jumpToArrayElement(i)
                hTarget = hArray.inputValue() # MDataHandle
                
                # -> node.target[i].targetWeight
                hWeight = hTarget.child(Flock.kTargetWeight) # MDataHandle
                targetWeights.append(hWeight.asFloat())
                
                # -> node.target[i].targetMatrix
                hMatrix = hTarget.child(Flock.kTargetMatrix) # MDataHandle
                transformMatrix = om.MTransformationMatrix(hMatrix.asMatrix()) # MTransformationMatrix
                targetPositions.append(transformMatrix.getTranslation(om.MSpace.kWorld)) # add to array of MVectors
                
                # -> node.target[i].targetRadius
                hFloat = hTarget.child(Flock.kTargetRadius) # MDataHandle
                targetRadii.append(hFloat.asFloat())
                
                # -> node.target[i].staticTarget
                hBool = hTarget.child(Flock.kStaticTarget) # MDataHandle
                targetStatic.append(hFloat.asBool())
                
                # -> node.target[i].targetArrivalDistance
                hFloat = hTarget.child(Flock.kTargetArrivalDistance) # MDataHandle
                targetArrivalDistances.append(hFloat.asFloat())
                
                # -> node.target[i].targetArrivalExponent
                hFloat = hTarget.child(Flock.kTargetArrivalExp) # MDataHandle
                targetArrivalExponents.append(hFloat.asFloat())
            
            # store initial target positions
            if len(targetPrevPositions) == 0:
                targetPrevPositions = targetPositions
                
            # Obstacles
            obstaclePositions = []
            obstacleBBoxes = [] # obstacle bounding boxes
            obstacleDistances = []
            obstacleAvoidanceDistances = [] # distances at which particles actually begin to avoid obstacles
            
            hArray = dataBlock.inputArrayValue(Flock.kObstacle) # MArrayDataHandle
            for i in range(hArray.elementCount()):
                # -> node.obstacle[i]
                hArray.jumpToArrayElement(i)
                hObstacle = hArray.inputValue() # MDataHandle
                
                # -> node.target[i].obstacleMatrix
                hMatrix = hObstacle.child(Flock.kObstacleMatrix) # MDataHandle
                transformMatrix = om.MTransformationMatrix(hMatrix.asMatrix()) # MTransformationMatrix
                obstaclePositions.append(transformMatrix.getTranslation(om.MSpace.kWorld)) # add to array of MVectors
                
                # -> node.target[i].obstacleBoundingBoxSize
                hBB = hObstacle.child(Flock.kObstacleBoundingBoxSize) # MDataHandle
                obstacleBBoxes.append(max(hBB.asDouble3()))
                
                # -> node.target[i].obstacleDistance
                hFloat = hObstacle.child(Flock.kObstacleDistance) # MDataHandle
                obstacleDistances.append(hFloat.asFloat())
                
                # -> node.target[i].obstacleAvoidanceDistances
                hFloat = hObstacle.child(Flock.kObstacleAvoidanceDistance) # MDataHandle
                obstacleAvoidanceDistances.append(hFloat.asFloat())

            #---------------------------------------------------------------------------------------------------#
            #                                      C. Compute output force
            #---------------------------------------------------------------------------------------------------#
            # TODO : add wander, flee, evasion etc.
            # TODO : optimize neighbour search speed
            # TODO : add ramps to control faloff
            # TODO : use exclusive targets (either A or B or C ...) based on which one has the most influence
            # TODO : jitter correction
            # TODO : implement Pursuit instead of Seek
            # TODO : use variable radius around obstacles
            # TODO : implement cylinder-like obstacle
            # TODO : speed-dependent FOV (smaller at larger speeds)
            # TODO : variable target influence depending on how far the particle is from it
            # TODO : etc...
            #---------------------------------------------------------------------------------------------------#
            
            ''' C.0 Force containers '''
            # output forces
            outForces = om.MVectorArray()
            outputF = om.MVector()
            
            # component forces
            cohesionF = om.MVector()
            alignmentF = om.MVector()
            separationF = om.MVector()
            
            # util arrays
            velocitiesN = normalize(velocities)
            
            # delta time
            # TODO: Support all other time units
            if deltaTime.unit() == 6: dt = 1.0 / 24 # MTime::kFilm (24fps)
            elif deltaTime.unit() == 7: dt = 1.0 / 25 # MTime::kPALFrame (25fps)             
            elif deltaTime.unit() == 8: dt = 1.0 / 30 # MTime::kNTSCFrame (30fps)
            else: dt = 1.0 / 24
            
            ''' C.1 Particle Iteration '''
            for i in range(count):
                '''----------------------------------------------------------------------------------------------
                C.1.0 Seek / Pursuit, Arrival
                TODO : add a brake coefficient for slowing down faster within arrival zone (?)
                ----------------------------------------------------------------------------------------------'''
                seekF = om.MVector()
                    
                for j in range(len(targetPositions)):
                    # check if target has any weight at all, skip if it doesn't
                    if targetWeights[j] == 0:
                        continue
                    
                    #----------------------------------------------------------------------------------------------#
                    # (SEEK) : vector to static target
                    #----------------------------------------------------------------------------------------------#
                    if targetStatic[j]:
                        vToTarget = targetPositions[j] - positions[i]
                    #----------------------------------------------------------------------------------------------#
                    # (PURSUIT) : vector to dynamic target
                    #----------------------------------------------------------------------------------------------#
                    else:
                        # we need to predict the future position of the target using a simple Euler approximation:
                        # https://www.khanacademy.org/math/differential-equations/first-order-differential-equations/eulers-method-tutorial/v/eulers-method
                        vTargetVelocity = targetPositions[j] - targetPrevPositions[j]
                        vTargetFuturePosition = targetPositions[j] + vTargetVelocity * dt
                        vToTarget = vTargetFuturePosition - positions[i]
                    
                    #----------------------------------------------------------------------------------------------#
                    # (COMMON) : distance tests
                    #----------------------------------------------------------------------------------------------#
                    distSquared = vToTarget * vToTarget
                    
                    # check if particle already reached the target
                    if distSquared < self.zeroThreshold:
                        continue

                    # check if particle is outside the target's influence radius (only if radius > 0)
                    if targetRadii[j] > 0 and distSquared > targetRadii[j] ** 2:
                        continue

                    #----------------------------------------------------------------------------------------------#
                    # (SEEK / PURSUIT) : if arrival distance is disabled or if particle is outside arrival distance
                    #----------------------------------------------------------------------------------------------#
                    if targetArrivalDistances[j] == 0 or distSquared > targetArrivalDistances[j] ** 2:                        
                        vToTarget.normalize()
                        vDesiredVelocity = vToTarget * maxSpeed

                    #----------------------------------------------------------------------------------------------#
                    # (ARRIVAL) : particle is inside target arrival distance (only if arrival distance > 0)
                    #----------------------------------------------------------------------------------------------#
                    else:
                        # distance to target
                        distance = vToTarget.length()
                        
                        # since we're inside the target arrival distance, 0 <= distance/targetArrivalDistance <= 1
                        falloff = pow(distance/targetArrivalDistances[j], targetArrivalExponents[j])
                        rampedSpeed = maxSpeed * falloff
                        
                        # equation below is equal to rampedSpeed * (vToTarget/distance), but with less operations.
                        # (1 division + 3 multiplications) vs. (3 divisions + 3 multiplications)
                        vDesiredVelocity = vToTarget * (rampedSpeed / distance)
                    
                    #----------------------------------------------------------------------------------------------#
                    # (COMMON) : Add the seek/arrival force for the current target.
                    #----------------------------------------------------------------------------------------------#
                    seekF += (vDesiredVelocity - velocities[i]) * targetWeights[j]

                '''----------------------------------------------------------------------------------------------
                C.1.1 Cohesion, Alignment, Separation
                ----------------------------------------------------------------------------------------------'''
                neighbours = [] # neighbours = [(id, vectorTo, distanceTo), ...]

                avgPos = om.MVector()
                avgVel = om.MVector()
                sepAccum = om.MVector()
                numSepNeighbours = findNeighbours(i, neighbours, positions, velocitiesN, maxNeighbours, cohesionDist, separationDist, fov)
                numCohNeighbours = len(neighbours)
                
                for index, neighbour in enumerate(neighbours):
                    # (for COHESION and ALIGNMENT) add to average neighbour position
                    avgPos += positions[neighbour[0]]
                    
                    # (for ALIGNMENT) add to average neighbour velocity
                    if index < numSepNeighbours : avgVel += velocities[neighbour[0]]
                    
                    # (for SEPARATION) add to separation force (!!! negative vector)
                    normalizedDistance = clampAndNormalizeDistance(neighbour[2], minCohesionDist, separationDist)
                    falloff = pow(normalizedDistance, forceExp)
                    
                    # equal to (neighbour[1]/neighbour[2]) * (1-falloff), but saves a few operations
                    sepAccum -= neighbour[1] * ((1 - falloff) / neighbour[2])
                
                # find average neigbour position and velocity
                if numCohNeighbours > 1: avgPos /= numCohNeighbours
                if numSepNeighbours > 1: avgVel /= numSepNeighbours
                
                #----------------------------------------------------------------------------------------------#
                # (COHESION)
                #----------------------------------------------------------------------------------------------#
                vToAveragePos = avgPos - positions[i] # vector to neighbours' avg position
                distance = vToAveragePos.length() # distance to neighbours' avg position
                vCohesionDirection = vToAveragePos / distance if distance > 0 else om.MVector()

                # falloff (linear if forceExp == 1)
                normalizedDistance = clampAndNormalizeDistance(distance, minCohesionDist, cohesionDist)
                falloff = pow(normalizedDistance, forceExp)
                cohesionF = vCohesionDirection * cohesionStrength * falloff
                
                #----------------------------------------------------------------------------------------------#
                # (ALIGNMENT) TODO: weighted individual contributions based on distance (use falloff)
                #----------------------------------------------------------------------------------------------#
                vAlignmentDirection = (avgVel - velocities[i]).normal() # normalized (alignment) vector
                alignmentF = vAlignmentDirection * alignmentStrength

                #----------------------------------------------------------------------------------------------#
                # (SEPARATION)
                #----------------------------------------------------------------------------------------------#
                separationF = sepAccum * separationStrength

                '''----------------------------------------------------------------------------------------------
                C.1.2 Obstacle avoidance
                ----------------------------------------------------------------------------------------------'''
                # iterate over obstacles and accumulate separation forces
                obstacleF = om.MVector()
                
                # add all the avoidance forces form all the obstacles
                for o in range(len(obstaclePositions)):
                    # get velocity direction
                    vN = velocitiesN[i]
                    
                    # first we test if the obstacle is behind the particle (using a dot product)
                    vToObstacle = obstaclePositions[o] - positions[i] # vector to obstacle's centre
                    if vToObstacle.normal() * vN <= 0:
                        continue
                    
                    vUp = vN ^ vToObstacle # vector perpendicular to velocity and to vToObstacle
                    vSide = (vN ^ vUp).normal() # unit vector perpendicular to velocity in the same plane as velocity and vToObstacle            
                    projSide = vToObstacle * vSide # length of vToObstacle projected onto the unit vector vSide
                    projForward = vToObstacle * vN # length of vToObstacle projected onto the unit velocity vector
                
                    # Check for possible collision. Two conditions to meet:
                    # 1. if length of side projection is < than the sum of the radii of the bounding spheres (obstacle + particle),
                    # plus whatever collision avoidance distance is set for that obstacle.
                    # 2. if length of forward projection is smaller than obstacleSight (aka the obstacle is in sight)
                    collisionZone = obstacleBBoxes[o]/2.0 + obstacleDistances[o]
                    if abs(projSide) < collisionZone and 0 < projForward < obstacleSight:
                        # Test if collision avoidance distance is enabled. Zero means that the particle will start avoiding
                        # the obstacle as soon as it sees it.
                        if obstacleAvoidanceDistances[o] == 0:
                            obstacleF += vSide
                        # Test if particle is less than collisionAvoidanceDistance away from the obstacle's
                        # collision zone, so it can start avoiding it
                        elif vToObstacle.length() < collisionZone + obstacleAvoidanceDistances[o]:
                            obstacleF += vSide
                
                # when a single collision object is present, the corrective force to avoid it is in the direction radiating from the
                # center of the collision obj, perpendicular to the current velocity vector. Essentially translates as side-movement.
                # multiple collision objects means all the forces are summed to get an avarage force vector.
                # We use maxAccel to avoid the objects as fast as possible.
                obstacleF.normalize()
                obstacleF *= maxAccel

                '''----------------------------------------------------------------------------------------------
                C.1.3 Aggregate all forces, check for speed and acceleration limits
                ----------------------------------------------------------------------------------------------'''
                # aggregate all forces
                outputF = seekF + cohesionF + alignmentF + separationF + obstacleF
                
                # check max acceleration (technically F = ma, but since mass == 1 => F = a) for next frame
                # (use squared values for efficiency - we skip taking a sqrt)
                if outputF * outputF > maxAccel ** 2:
                    outputF.normalize()
                    outputF *= maxAccel
                
                # find velocity for next frame (velocityFuture = velocityCurrent + force*dt)
                # we don't have access to Maya's internal integrator(do we?), so we can only approximate the velocity
                # at the next frame by doing a simple Euler integration step ourselves, using the passed in delta time.
                velNextFrame = velocities[i] + outputF * dt

                # check speed (velocity's magnitude) for next frame
                # (use squared values for efficiency - we skip taking a sqrt)
                if velNextFrame * velNextFrame > maxSpeed ** 2:
                    # compute what would be (on the next frame) the velocity vector with the speed limit active
                    velNextFrame.normalize()
                    velNextFrame *= maxSpeed

                    # get difference vector between current velocity and future velocity
                    outputF = velNextFrame - velocities[i]
                    
                    # set outputF to a value that takes the particle down to maxSpeed right on the next frame
                    # (otherwise speed is gradually decreased over time by Maya's integrator)
                    if speedJump:
                        outputF *= 24
                
                '''----------------------------------------------------------------------------------------------
                C.1.4 Append the final force to the array
                ----------------------------------------------------------------------------------------------'''
                outForces.append(outputF)

            #---------------------------------------------------------------------------------------------------#
            #                                      D. Set output force plug
            #---------------------------------------------------------------------------------------------------#
            
            ''' D.0 Get output data handle and use an array builder '''
            try: hOutArray = dataBlock.outputArrayValue(plugOutputForce) # MArrayDataHandle
            except: statusError('ERROR in hOutArray = dataBlock.outputArrayValue().')
            try: bOutArray = hOutArray.builder() # MArrayDataBuilder
            except: statusError('ERROR in bOutArray = hOutArray.builder().')

            ''' D.1 Get output force array from dataBlock '''
            try: hOut = bOutArray.addElement(multiIndex) # MDataHandle
            except: statusError('ERROR in hOut = bOutArray.addElement().')

            fnOutputForce = om.MFnVectorArrayData() # MFnVectorArrayData
            try: oOutputForce = fnOutputForce.create(outForces) # MObject
            except: statusError('ERROR in oOutputForce = fnOutputForce.create().')

            ''' D.2 Update data block with new output force data. '''
            hOut.setMObject(oOutputForce)
            dataBlock.setClean(plug)

    @classmethod
    def nodeCreator(cls):
        return ommpx.asMPxPtr(cls())
        
    @classmethod
    def nodeInitializer(cls):
        ''' 1. Create attribute function sets '''
        nAttr = om.MFnNumericAttribute()
        tAttr = om.MFnTypedAttribute()
        mAttr = om.MFnMatrixAttribute()
        cAttr = om.MFnCompoundAttribute()
        rAttr = om.MRampAttribute()
        
        ''' 2. Set attributes '''
        # min / max values
        cls.kMinCohesionDist = nAttr.create(cls.kMinCohesionDistLong, cls.kMinCohesionDistShort, om.MFnNumericData.kFloat, 0.0)
        nAttr.setMin(0.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kCohesionDist = nAttr.create(cls.kCohesionDistLong, cls.kCohesionDistShort, om.MFnNumericData.kFloat, 3.0)
        nAttr.setMin(0.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kMaxNeighbours = nAttr.create(cls.kMaxNeighboursLong, cls.kMaxNeighboursShort, om.MFnNumericData.kInt, 6)
        nAttr.setMin(-1)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kMaxSpeed = nAttr.create(cls.kMaxSpeedLong, cls.kMaxSpeedShort, om.MFnNumericData.kFloat, 25.0)
        nAttr.setMin(0.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kSpeedJump = nAttr.create(cls.kSpeedJumpLong, cls.kSpeedJumpShort, om.MFnNumericData.kBoolean, 0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kMaxAcceleration = nAttr.create(cls.kMaxAccelerationLong, cls.kMaxAccelerationShort, om.MFnNumericData.kFloat, 30.0)
        nAttr.setMin(0.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kSeparationDist = nAttr.create(cls.kSeparationDistLong, cls.kSeparationDistShort, om.MFnNumericData.kFloat, 1.0)
        nAttr.setMin(0.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kFov = nAttr.create(cls.kFovLong, cls.kFovShort, om.MFnNumericData.kFloat, 360.0)
        nAttr.setMin(0.0)
        nAttr.setMax(360.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        # force falloff
        cls.kForceFalloffExponent = nAttr.create(cls.kForceFalloffExponentLong, cls.kForceFalloffExponentShort, om.MFnNumericData.kFloat, 1.0)
        nAttr.setMin(0.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        # force strength
        cls.kCohesionStrength = nAttr.create(cls.kCohesionStrengthLong, cls.kCohesionStrengthShort, om.MFnNumericData.kFloat, 1.0)
        nAttr.setMin(0.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kAlignmentStrength = nAttr.create(cls.kAlignmentStrengthLong, cls.kAlignmentStrengthShort, om.MFnNumericData.kFloat, 1.0)
        nAttr.setMin(0.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kSeparationStrength = nAttr.create(cls.kSeparationStrengthLong, cls.kSeparationStrengthShort, om.MFnNumericData.kFloat, 10.0)
        nAttr.setMin(0.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        # target        
        cls.kTargetMatrix = mAttr.create(cls.kTargetMatrixLong, cls.kTargetMatrixShort, om.MFnMatrixAttribute.kDouble)
        mAttr.setStorable(True)
        mAttr.setKeyable(True)
        
        cls.kTargetWeight = nAttr.create(cls.kTargetWeightLong, cls.kTargetWeightShort, om.MFnNumericData.kFloat, 1.0)
        nAttr.setMin(0.0)
        nAttr.setMax(1.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kTargetRadius = nAttr.create(cls.kTargetRadiusLong, cls.kTargetRadiusShort, om.MFnNumericData.kFloat, 0.0)
        nAttr.setMin(0.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kStaticTarget = nAttr.create(cls.kStaticTargetLong, cls.kStaticTargetShort, om.MFnNumericData.kBoolean, True)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kTargetArrivalDistance = nAttr.create(cls.kTargetArrivalDistanceLong, cls.kTargetArrivalDistanceShort, om.MFnNumericData.kFloat, 0.0)
        nAttr.setMin(0.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kTargetArrivalExp = nAttr.create(cls.kTargetArrivalExpLong, cls.kTargetArrivalExpShort, om.MFnNumericData.kFloat, 2.0)
        nAttr.setMin(0.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kTarget = cAttr.create(cls.kTargetLong, cls.kTargetShort)
        cAttr.setStorable(True)
        cAttr.setKeyable(True)
        cAttr.setArray(True)
        cAttr.addChild(cls.kTargetMatrix)
        cAttr.addChild(cls.kTargetWeight)
        cAttr.addChild(cls.kTargetRadius)
        cAttr.addChild(cls.kStaticTarget)
        cAttr.addChild(cls.kTargetArrivalDistance)
        cAttr.addChild(cls.kTargetArrivalExp)
        cAttr.setDisconnectBehavior(om.MFnGenericAttribute.kDelete)
        
        # obstacles
        cls.kObstacleMatrix = mAttr.create(cls.kObstacleMatrixLong, cls.kObstacleMatrixShort, om.MFnMatrixAttribute.kDouble)
        mAttr.setStorable(True)
        mAttr.setKeyable(True)
        
        cls.kObstacleBoundingBoxSize = nAttr.create(cls.kObstacleBoundingBoxSizeLong, cls.kObstacleBoundingBoxSizeShort, om.MFnNumericData.k3Double)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kObstacleDistance = nAttr.create(cls.kObstacleDistanceLong, cls.kObstacleDistanceShort, om.MFnNumericData.kFloat, 0.5)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kObstacleAvoidanceDistance = nAttr.create(cls.kObstacleAvoidanceDistanceLong, cls.kObstacleAvoidanceDistanceShort, om.MFnNumericData.kFloat, 0.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        cls.kObstacle = cAttr.create(cls.kObstacleLong, cls.kObstacleShort)
        cAttr.setArray(True)
        cAttr.setStorable(True)
        cAttr.setKeyable(True)
        cAttr.addChild(cls.kObstacleMatrix)
        cAttr.addChild(cls.kObstacleBoundingBoxSize)
        cAttr.addChild(cls.kObstacleDistance)
        cAttr.addChild(cls.kObstacleAvoidanceDistance)
        cAttr.setDisconnectBehavior(om.MFnGenericAttribute.kDelete)
        
        cls.kObstacleSight = nAttr.create(cls.kObstacleSightLong, cls.kObstacleSightShort, om.MFnNumericData.kFloat, 10.0)
        nAttr.setStorable(True)
        nAttr.setKeyable(True)
        
        # particle id
        cls.kParticleId = tAttr.create(cls.kParticleIdLong, cls.kParticleIdShort, om.MFnData.kDoubleArray)
        
        ''' 3. Add attributes '''
        # add [in]
        cls.addAttribute(cls.kMinCohesionDist)
        cls.addAttribute(cls.kCohesionDist)
        cls.addAttribute(cls.kMaxNeighbours)
        cls.addAttribute(cls.kMaxSpeed)
        cls.addAttribute(cls.kSpeedJump)
        cls.addAttribute(cls.kMaxAcceleration)
        cls.addAttribute(cls.kSeparationDist)
        cls.addAttribute(cls.kFov)
        cls.addAttribute(cls.kForceFalloffExponent)
        cls.addAttribute(cls.kCohesionStrength)
        cls.addAttribute(cls.kAlignmentStrength)
        cls.addAttribute(cls.kSeparationStrength)
        cls.addAttribute(cls.kTarget)
        cls.addAttribute(cls.kObstacle)
        cls.addAttribute(cls.kObstacleSight)
        cls.addAttribute(cls.kParticleId)
        
        ''' 4. Connect attributes '''
        cls.attributeAffects(cls.kMinCohesionDist, ommpx.cvar.MPxFieldNode_mOutputForce)
        cls.attributeAffects(cls.kCohesionDist, ommpx.cvar.MPxFieldNode_mOutputForce)
        cls.attributeAffects(cls.kMaxNeighbours, ommpx.cvar.MPxFieldNode_mOutputForce)
        cls.attributeAffects(cls.kMaxSpeed, ommpx.cvar.MPxFieldNode_mOutputForce)
        cls.attributeAffects(cls.kSpeedJump, ommpx.cvar.MPxFieldNode_mOutputForce)
        cls.attributeAffects(cls.kMaxAcceleration, ommpx.cvar.MPxFieldNode_mOutputForce)
        cls.attributeAffects(cls.kSeparationDist, ommpx.cvar.MPxFieldNode_mOutputForce)
        cls.attributeAffects(cls.kForceFalloffExponent, ommpx.cvar.MPxFieldNode_mOutputForce)
        cls.attributeAffects(cls.kCohesionStrength, ommpx.cvar.MPxFieldNode_mOutputForce)
        cls.attributeAffects(cls.kAlignmentStrength, ommpx.cvar.MPxFieldNode_mOutputForce)
        cls.attributeAffects(cls.kSeparationStrength, ommpx.cvar.MPxFieldNode_mOutputForce)
        cls.attributeAffects(cls.kTarget, ommpx.cvar.MPxFieldNode_mOutputForce)
        cls.attributeAffects(cls.kObstacle, ommpx.cvar.MPxFieldNode_mOutputForce)
        cls.attributeAffects(cls.kObstacleSight, ommpx.cvar.MPxFieldNode_mOutputForce)
        cls.attributeAffects(cls.kParticleId, ommpx.cvar.MPxFieldNode_mOutputForce)

def initializePlugin(obj):
    plugin = ommpx.MFnPlugin(obj, 'George Bitiusca', '1.0', 'Any')
    try:
        plugin.registerNode(
            Flock.kNodeName,
            Flock.kNodeId,
            Flock.nodeCreator,
            Flock.nodeInitializer,
            ommpx.MPxNode.kFieldNode)
    except:
        raise Exception('Failed to register node:{node}'.format(node = Flock.kNodeName))
        
def uninitializePlugin(obj):
    plugin = ommpx.MFnPlugin(obj)
    try:
        plugin.deregisterNode(Flock.kNodeId)
    except:
        raise Exception('Failed to unregister node: {node}'.format(node = Flock.kNodeName))
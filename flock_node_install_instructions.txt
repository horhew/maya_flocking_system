Installation instructions:

1. Make sure flock_node.py is in Maya's plug-in search path, usually that's done through the maya.env file.
2. Execute flock_node_setup.py in Maya's Script Editor (CTRL+C everything, paste, then execute). That will build a basic scene for the flocking system.
3. Alternatively, you can open the provided .ma file, which contains a slightly more complex scene.
4. There is no interface...sorry. You'll have to figure stuff out from the Attribute Editor and by reading the comments in the .py file.
5. That's it.

-----
Known limitations:
No interface unfortunately. Too much to do, too little time.

Not optimized, thus runs pretty slow when even a moderate number of particles are involved. I recommend keeping their number below 100. Porting to C++ and code optimization will likely increase performace a lot.
Many behaviours were not coded due to lack of time, but will surely be added during further development. For the moment and for demo purposes, all the essential forces are in place though.

Any questions, please send me an e-mail: george@georgebit.com
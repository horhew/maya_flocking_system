## Particle System plugin for Maya

Plugin based on Craig W. Reynolds' famous flocking behaviour paper. This plugin was written in Python using the Maya API and it's essentially a force field node (inherits from MPxFieldNode). Apart from the core forces of cohesion, separation and alignment, I also implemented multi-target seek, arrival, pursuit and multi-obstacle avoidance. Due to time constraints (couple of weeks), I didn't get around writing the C++ equivalent, so performance is definitely not the strong point of the plugin. Nevertheless I had great fun working on this project and I look forward to extending it and translating it to C++ in the future.

Installation instructions in the txt file.